<?php

namespace App\Model\DB\Product;

use App\Model\DB\Category\Category;
use App\Model\DB\Foto\Foto;
use App\Model\DB\ItemCart\ItemCart;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */

class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $news = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $recommended = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disabled = false;

    /**
     * Many Products have One Category.
     * @ORM\ManyToOne(targetEntity="App\Model\DB\Category\Category")
     * @var Category
     */
    private $category;

    /**
     * One Product have Many Fotos.
     * @ORM\OneToMany(targetEntity="App\Model\DB\Foto\Foto", mappedBy="product")
     * @var Foto
     */
    private $foto;

    /**
     * One Product have Many ItemsCart.
     * @ORM\OneToMany(targetEntity="App\Model\DB\ItemCart\ItemCart", mappedBy="product")
     * @var ItemCart
     */
    private $itemCart;


    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Foto
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ItemCart
     */
    public function getItemCart()
    {
        return $this->itemCart;
    }

    /**
     * @param ItemCart $itemCart
     */
    public function setItemCart($itemCart)
    {
        $this->itemCart = $itemCart;
    }

}