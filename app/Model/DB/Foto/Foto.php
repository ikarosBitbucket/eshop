<?php

namespace App\Model\DB\Foto;

use App\Model\DB\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */

class Foto
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $path;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disabled = false;


    /**
     * Many Foto have One Product.
     * @ORM\ManyToOne(targetEntity="App\Model\DB\Product\Product", inversedBy="foto")
     * @var Product
     */
    private $product;


    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

}