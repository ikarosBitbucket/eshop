<?php

namespace App\Model\DB\Ordered;


use App\Model\DB\ItemCart\ItemCart;
use App\Model\DB\State\State;
use App\Model\System\Constants;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */

class Ordered
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * Many Order have One State.
     * @ORM\ManyToOne(targetEntity="App\Model\DB\State\State")
     * @var State
     */
    private $state;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $surname = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $address = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $country = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;


    /**
     * One Order have Many ItemsCart.
     * @ORM\OneToMany(targetEntity="App\Model\DB\ItemCart\ItemCart", mappedBy="ordered")
     * @var ItemCart
     */
    private $itemCart;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return State
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param State $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return ItemCart
     */
    public function getItemCart()
    {
        return $this->itemCart;
    }

    /**
     * @param ItemCart $itemCart
     */
    public function setItemCart($itemCart)
    {
        $this->itemCart = $itemCart;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

}