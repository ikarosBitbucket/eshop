<?php

namespace App\Model\DB\ItemCart;

use App\Model\DB\Ordered\Ordered;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */

class ItemCart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * Many ItemsCart have One Ordered.
     * @ORM\ManyToOne(targetEntity="App\Model\DB\Ordered\Ordered", inversedBy="itemCart")
     * @var Ordered
     */
    private $ordered;

    /**
     * Many ItemsCart have One Product.
     * @ORM\ManyToOne(targetEntity="App\Model\DB\Product\Product", inversedBy="itemCart")
     * @var Product
     */
    private $product;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

    /**
     * @param mixed $ordered
     */
    public function setOrdered($ordered)
    {
        $this->ordered = $ordered;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

}