<?php

namespace App\Model\System;

/**
 * Description of Constants
 *
 * @author Miroslav
 */
class Constants
{

    const DATE_FORMAT_SAVE = "Y-m-d";
    const DATE_FORMAT = "d.m.Y";
    const TIME_FORMAT = "d.m.Y H:i";
    const TIME_FORMAT_SAVE = "Y-m-d H:i";
    const VAT = 21;
    const STATE_NEW = 1;
    const STATE_DONE = 2;

}
