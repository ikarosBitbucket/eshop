<?php

namespace App\Presenters;
use Nette\Neon\Exception;
use Tracy\Debugger;

/**
 * Base presenter
 */
abstract class BasePresenter extends \Nette\Application\UI\Presenter
{

    public function beforeRender()
    {
        parent::beforeRender();
        if ($this->isAjax()) {
            $this->redrawControl('flashMessages');
        }
    }


}
