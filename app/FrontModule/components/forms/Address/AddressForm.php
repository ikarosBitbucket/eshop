<?php

namespace App\FrontModule\Components\Forms\Address;

use App\Model\DB\Ordered\Ordered;
use Nette\Application\UI\Form;

class AddressForm extends Form  {

    const ORDERED_OLD = "ordered_old";

    /*
     * @var AddressFormController
     */
    private $addressFormController;

    /*
     * @var Ordered
     */
    private $ordered;

    function __construct(Ordered $ordered, AddressFormController $addressFormController) {
        parent::__construct();
        $this->addressFormController = $addressFormController;
        $this->ordered = $ordered;
        $this->populate();
    }

    protected function populate() {

        $this->addText('name')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'Jméno nesmí být delší než %d znaků.', 40);

        $this->addText('surname')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'Příjmení nesmí být delší než %d znaků.', 40);

        $this->addText('phone')
            ->setRequired(false)
            ->addRule(Form::PATTERN, 'Zadejte telefon ve správném formátu', '\+?\d{9,12}');

        $this->addText('email')
            ->setRequired()
            ->addRule(Form::EMAIL, 'Musíte zadat e-mailovou adresu ve správném tvaru.')
            ->addRule(Form::MAX_LENGTH, 'E-mail nesmí být delší než %d znaků.', 80);

        $this->addText('country')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'Stát nesmí být delší než %d znaků.', 50);

        $this->addText('city')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'Město nesmí být delší než %d znaků.', 50);

        $this->addText('address')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'Adresa nesmí být delší než %d znaků.', 100);

        $this->addCheckbox('agree')
            ->setRequired();

        $this->addSubmit("send", "Odeslat objednávku ");

        $this->onSuccess[] = array($this, 'process');
        return $this;
    }


    public function process(Form $form, $values) {

        $values['ordered'] = $this->ordered;
        $this->addressFormController->send($values);
		$this->getPresenter()->getSession(self::ORDERED_OLD)->id = $this->ordered->getId();
        $this->getPresenter()->redirect("ThankYou:");
    }

}
