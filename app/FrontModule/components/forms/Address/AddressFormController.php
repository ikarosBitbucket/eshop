<?php

namespace App\FrontModule\Components\Forms\Address;

use App\Model\DB\Ordered\Ordered;
use App\Model\DB\State\State;
use App\Model\System\Constants;
use Doctrine\ORM\EntityManager;
use Nette\Utils\DateTime;

class AddressFormController {

    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @var EntityRepository
     */
    public $ordered;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

    }

    public function send ($data) {

        unset($data['agree']);
        $ordered = $data['ordered'];
        unset($data['ordered']);
        $this->save($data, $ordered);
    }

    private function save($data, $ordered) {

        $stateRepository = $this->entityManager->getRepository(State::class);
        $ordered->setState($stateRepository->find(Constants::STATE_DONE));
        $ordered->setName($data['name']);
        $ordered->setSurname($data['surname']);
        $ordered->setCity($data['city']);
        $ordered->setPhone($data['phone']);
        $ordered->setCountry($data['country']);
        $ordered->setDate(new \DateTime());
        $ordered->setAddress($data['address']);
        $ordered->setEmail($data['email']);
        $this->entityManager->merge($ordered);
        $this->entityManager->flush();
    }
}