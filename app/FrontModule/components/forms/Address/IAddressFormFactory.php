<?php

namespace App\FrontModule\Components\Forms\Address;


use App\Model\DB\Ordered\Ordered;

interface IAddressFormFactory
{

    /**
     * @return AddressForm
     */
    public function create(Ordered $ordered);

}
