<?php

namespace App\FrontModule\Presenters;


use App\FrontModule\Components\Forms\Address\IAddressFormFactory;
use App\Model\DB\Ordered\Ordered;

class AddressPresenter extends BasePresenter
{

    /**
     * @inject
     * @var IAddressFormFactory
     */
    public $addressFormFactory;

    /**
     * @var Ordered
     */
    private $ordered;

    public function startup()
    {
        parent::startup();
        $this->ordered = $this->orderedRepository->find($this->getOrderedId());
    }

    public function renderDefault(){

        $this->template->ordered = $this->ordered;
    }

    protected function createComponentAddressForm()
    {
        return $this->addressFormFactory->create($this->ordered);
    }


}
