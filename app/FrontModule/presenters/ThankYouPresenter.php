<?php

namespace App\FrontModule\Presenters;


use App\FrontModule\Components\Forms\Address\AddressForm;

class ThankYouPresenter extends BasePresenter
{

    public function renderDefault()
    {

        if ($this->getSession(AddressForm::ORDERED_OLD)->id) {
            $this->template->orderedDone = $this->orderedRepository->find($this->getSession(AddressForm::ORDERED_OLD)->id);
        }
        $this->template->ordered = $this->orderedRepository->find($this->getOrderedId());
    }


}
