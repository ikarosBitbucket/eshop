<?php

namespace App\FrontModule\Presenters;

class CheckoutPresenter extends BasePresenter
{

    public function renderDefault(){

        $this->template->ordered = $this->orderedRepository->find($this->getOrderedId());
    }


}
