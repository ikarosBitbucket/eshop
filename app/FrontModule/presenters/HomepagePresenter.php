<?php

namespace App\FrontModule\Presenters;

class HomepagePresenter extends BasePresenter
{

    public function renderDefault(){
        $product = $this->productRepository;
        $this->template->news = $product->findBy(["news"=> true]);
        $this->template->recommended = $product->findBy(["recommended"=> true]);
        $this->template->ordered = $this->orderedRepository->find($this->getOrderedId());
    }
}
