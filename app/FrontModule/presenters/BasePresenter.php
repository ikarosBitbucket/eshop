<?php

namespace App\FrontModule\Presenters;

use App\Model\DB\ItemCart\ItemCart;
use App\Model\DB\Ordered\Ordered;
use App\Model\DB\Product\Product;
use App\Model\DB\State\State;
use App\Model\System\Constants;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette\Utils\DateTime;
use Tracy\Debugger;


/**
 * Base presenter
 */
abstract class BasePresenter extends \App\Presenters\BasePresenter
{
    const POPUP = "popup";
    const ORDERED = "ordered";

    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @var EntityRepository
     */
    public $orderedRepository;

    /**
     * @var EntityRepository
     */
    public $itemCartRepository;

    /**
     * @var EntityRepository
     */
    public $stateRepository;

    /**
     * @var EntityRepository
     */
    public $productRepository;


    public function startup()
    {
        parent::startup();
        $this->orderedRepository = $this->entityManager->getRepository(Ordered::class);
        $this->itemCartRepository = $this->entityManager->getRepository(ItemCart::class);
        $this->stateRepository = $this->entityManager->getRepository(State::class);
        $this->productRepository = $this->entityManager->getRepository(Product::class);
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->popupSession = $this->getPopupSession();

        if ($this->isAjax()) {
            $this->redrawControl('itemCart');
            $this->redrawControl('menu');

        }
    }

    public function getPopupSession()
    {
        return $this->getSession(self::POPUP)->popup_state;
    }

    public function setPopupSession($state)
    {
        $this->getSession(self::POPUP)->popup_state = $state;
    }

    public function handlePopupSession()
    {

        $this->setPopupSession(true);
    }

    public function handleAddToCart($id)
    {

        $orderedId = $this->getOrderedId();

        if ($itemCart = $this->itemCartIsExist($orderedId, $id)) {
            $this->updateItemCartCount($itemCart, $itemCart->getCount() + 1);
        } else {
            $this->setNewCartItem($orderedId, $id, 1);
        }
        $this->updateOrderedPrice($this->orderedRepository->find($orderedId));
        $this->redirect("Checkout:");

    }

    public function handleRemoveItemCart($id)
    {
        $itemCart = $this->itemCartRepository->find($id);
        if ($itemCart && $itemCart->getOrdered()->getId() == $this->getOrderedSession()) {
            $ordererId = $itemCart->getOrdered();
            $this->entityManager->remove($itemCart);
            $this->entityManager->flush();
            $this->updateOrderedPrice($ordererId);
        }

    }

    public function handleChangeCount($id, $change, $plus = true){

        $itemCart = $this->itemCartRepository->find($id);
        if ($itemCart->getOrdered()->getId() == $this->getOrderedSession() && ($itemCart->getCount() > 0 || $plus)) {
            if($plus){
               $count = $itemCart->getCount() + $change;
            }
            else{
                $count = $itemCart->getCount() - $change;
            }

            $itemCart->setCount($count);
            $this->entityManager->merge($itemCart);
            $this->entityManager->flush();
            $this->updateOrderedPrice($itemCart->getOrdered());
        }
    }

    public function getOrderedSession()
    {

        $id = $this->getSession(self::ORDERED)->id;

        if ($id && $this->orderedRepository->findBy(['id' => $id, 'state' => Constants::STATE_NEW])) {
            return $id;
        }
        return false;
    }

    public function setOrderedSession($id)
    {
        $this->getSession(self::ORDERED)->id = $id;
    }

    public function setNewCartItem($orderedId, $productId, $count)
    {
        $itemCart = new ItemCart();
        $itemCart->setOrdered($this->orderedRepository->find($orderedId));
        $itemCart->setProduct($this->productRepository->find($productId));
        $itemCart->setCount($count);
        $this->entityManager->persist($itemCart);
        $this->entityManager->flush();
    }

    public function setNewOrdered($price = 0, $state = Constants::STATE_NEW)
    {

        $ordered = new Ordered();
        $ordered->setPrice($price);
        $ordered->setDate(new \DateTime());
        $ordered->setState($this->stateRepository->find($state));
        $this->entityManager->persist($ordered);
        $this->entityManager->flush();
        return $ordered->getId();
    }


    public function getOrderedId()
    {
        if ($this->getOrderedSession()) {
            return $this->getOrderedSession();
        }

        $orderedId = $this->setNewOrdered();
        $this->setOrderedSession($orderedId);
        return $orderedId;

    }

    public function itemCartIsExist($orderedId, $productId)
    {
        return $this->itemCartRepository->findOneBy(["product" => $productId, "ordered" => $orderedId]);
    }

    public function updateItemCartCount($itemCart, $count)
    {

        $itemCart->setCount($count);
        $this->entityManager->merge($itemCart);
        $this->entityManager->flush();
    }

    public function updateOrderedPrice($ordered)
    {

        $ordered->setPrice($this->sumOrderedPrice($ordered->getId()));
        $this->entityManager->merge($ordered);
        $this->entityManager->flush();
    }

    public function sumOrderedPrice($orderedId)
    {
        $itemsCart = $this->itemCartRepository->findBy(["ordered" => $orderedId]);
        $price = 0;
        foreach ($itemsCart as $item) {
            $price += $item->getProduct()->getPrice() * $item->getCount();
        }
        return $price;
    }
}


